package Powerups;

public class Powerup {
    private double hpMultipler;
    private double damageMultipler;
    private int shots;

    private Powerup() {
    }

    private Powerup(DamageMultipler damageMultipler) {
        if (damageMultipler == null)
            return;
        this.damageMultipler = damageMultipler.damageMultipler;
        shots = damageMultipler.shots;
    }

    private Powerup(HpMultipler hpMultipler) {
        if (hpMultipler == null)
            return;
        this.hpMultipler = hpMultipler.hpMultipler;
    }

    public int getShots() {
        return shots;
    }

    public void decrementShots() {
        shots--;
    }

    public double getHpMultipler() {
        return hpMultipler;
    }

    public double getDamageMultipler() {
        return damageMultipler;
    }

    public static class DamageMultipler {
        private final double damageMultipler;
        private int shots;

        public DamageMultipler(double damageMultipler) {
            this.damageMultipler = damageMultipler;
        }

        public DamageMultipler shots(int shots) {
            this.shots = shots;
            return this;
        }

        public Powerup build() {
            return new Powerup(this);
        }
    }

    public static class HpMultipler {
        private final double hpMultipler;

        public HpMultipler(double hpMultipler) {
            this.hpMultipler = hpMultipler;
        }

        public Powerup build() {
            return new Powerup(this);
        }
    }
}
