package Segment;

import com.googlecode.lanterna.Symbols;

public class SegmentDestructible extends SegmentT {
    private  int hp;
    public SegmentDestructible(int x, int y) {
        super(x, y);
        charOfBlock= Symbols.DIAMOND;
        hp=1000;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}
