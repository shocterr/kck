package Segment;

import com.googlecode.lanterna.Symbols;

public class SegmentIndestructible extends SegmentT {
    public SegmentIndestructible(int x, int y) {
        super(x, y);
        charOfBlock= Symbols.SOLID_SQUARE;
    }
}
