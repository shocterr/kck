package Segment;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

import java.awt.*;

public class SegmentT {
    protected char charOfBlock;
    protected int positionX;
    protected int positionY;
    protected int W;
    protected int H;

    public SegmentT(int x, int y) {
        this.positionX = x;
        this.positionY = y;
        W = 2;
        H = 2;
        charOfBlock=' ';
    }

    public Rectangle getBounds() {
        return new Rectangle(positionX,positionY, W, H);

    }

    public void draw(TextGraphics textGraphics) {
        textGraphics.drawRectangle(new TerminalPosition(positionX,positionY),new TerminalSize(W,H), charOfBlock);
    }

    public void tick() {
    }

    //public void collisionV(Sprite sprite) {
    //}

    //public void collisionH(Sprite sprite) {
   // }

}
