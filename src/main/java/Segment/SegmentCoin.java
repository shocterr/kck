package Segment;

import com.googlecode.lanterna.Symbols;



public class SegmentCoin extends SegmentT {
    private boolean visted=false;


    public SegmentCoin(int x, int y) {
        super(x, y);
        charOfBlock= Symbols.TRIANGLE_DOWN_POINTING_MEDIUM_BLACK;
    }

    public boolean isVisted() {
        return visted;
    }

    public void setVisted() {
        this.visted = true;
    }
}
