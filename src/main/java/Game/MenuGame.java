package Game;

import java.util.ArrayList;
import java.util.List;

public class MenuGame {
    private List<Option> menuList;
    public MenuGame(){
        menuList = new ArrayList<Option>() {{
            add(new Option("INSTRUTION"));
            add(new Option("PLAY"));
            add(new Option("INFORMATION"));
            add(new Option("EXIT"));
        }};

    }

    public List<Option> getMenuList() {
        return menuList;
    }
}
