package Game;

import Segment.*;
import Tank.TankSprite;
import com.googlecode.lanterna.Symbols;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.googlecode.lanterna.input.KeyType.Escape;

public class Game {
    //private Stage stage;
    private List<SegmentT> board;
    private final int TILESIZE = 2;
    private TankSprite sprite1;
    private Terminal terminal;
    private TextGraphics textGraphics;
    private boolean painted = false;
    private final int terminalH = 30;
    private final int terminalW = 50;
    private boolean running = true;


    public void createMenu() {
        boolean keepRunning = true;
        MenuGame menuGame = new MenuGame();
        List<Option> menuList = menuGame.getMenuList();
        int length;
        int size = menuList.size();
        int cursorPosition = 12;

        for (Option option : menuList) {
            length = option.getName().length();
            option.setPositionX((terminalW - length) / 2);
            option.setPositionY(cursorPosition);
            textGraphics.putString(option.getPositionX(), cursorPosition, option.getName());
            cursorPosition++;
        }
        manageMenu(menuGame);
    }

    public void exitMenu() {
        try {
            terminal.clearScreen();
            long time = sprite1.getEndTime();
            String endTime = "Your time: " + String.valueOf(time) + " seconds";
            textGraphics.putString((terminalW - endTime.length()) / 2, (terminalH - 4) / 2, endTime);
            int coins = sprite1.getPoints();
            String coinsText = "Your coins: " + String.valueOf(coins);
            textGraphics.putString((terminalW - coinsText.length()) / 2, ((terminalH - 4) / 2) + 1, coinsText);
            int points = (int) (coins + time / 2);
            String pointsText = "Your points: " + String.valueOf(points);
            textGraphics.putString((terminalW - pointsText.length()) / 2, ((terminalH - 4) / 2) + 2, pointsText);
            String proceed = "Click ESC to proceed";
            textGraphics.putString((terminalW - proceed.length()) / 2, ((terminalH - 4) / 2) + 3, proceed);
            terminal.flush();
            if (terminal.readInput().getKeyType().equals(Escape)) {
                running = false;
                terminal.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void manageMenu(MenuGame menuGame) {
        int i = 0;
        int size = menuGame.getMenuList().size();
        boolean keepRunning = true;
        Option currentOption = menuGame.getMenuList().get(0);
        while (keepRunning) {
            KeyStroke key = null;

            try {

                textGraphics.drawRectangle(new TerminalPosition(currentOption.getPositionX() - 1, currentOption.getPositionY()), new TerminalSize(1, 1), Symbols.ARROW_RIGHT);
                terminal.flush();
                key = terminal.readInput();
                switch (key.getKeyType()) {
                    case ArrowUp: {
                        textGraphics.drawRectangle(new TerminalPosition(currentOption.getPositionX() - 1, currentOption.getPositionY()), new TerminalSize(1, 1), ' ');
                        if (i == 0) {
                            i = size - 1;
                            currentOption = menuGame.getMenuList().get(i);
                        } else {
                            i--;
                            currentOption = menuGame.getMenuList().get(i);
                        }

                        textGraphics.drawRectangle(new TerminalPosition(currentOption.getPositionX() - 1, currentOption.getPositionY()), new TerminalSize(1, 1), Symbols.ARROW_RIGHT);
                        terminal.flush();


                        break;
                    }


                    case ArrowDown: {
                        textGraphics.drawRectangle(new TerminalPosition(currentOption.getPositionX() - 1, currentOption.getPositionY()), new TerminalSize(1, 1), ' ');
                        if (i == size - 1) {
                            i = 0;
                            currentOption = menuGame.getMenuList().get(i);
                        } else {
                            i++;
                            currentOption = menuGame.getMenuList().get(i);
                        }


                        textGraphics.drawRectangle(new TerminalPosition(currentOption.getPositionX() - 1, currentOption.getPositionY()), new TerminalSize(1, 1), Symbols.ARROW_RIGHT);
                        terminal.flush();
                        break;
                    }
                    case Enter: {
                        if (currentOption.getName().equals("PLAY")) {
                            terminal.clearScreen();
                            paint();
                        }
                        if (currentOption.getName().equals("Exit")) {
                            keepRunning = false;
                            terminal.close();
                        }

                    }

                    case Escape: {
                        keepRunning = false;
                        terminal.close();
                    }


                }

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    public List<SegmentT> createBoard(String filePath) {

        BufferedReader br = null;

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(Objects.requireNonNull(classLoader.getResource(filePath)).getFile());
            br = new BufferedReader(new FileReader(file));

            List<SegmentT> board = new ArrayList<>();
            String linia;

            int x;
            int y = 4;
            int liczba;
            int znaki;
            char znak;
            char cyfra1;
            char cyfra2;
            int licznik = 0;
            int sx = 0, sy = 0, boardX = 0, boardY = 0;
            while ((linia = br.readLine()) != null) {
                if (licznik == 0) {
                    String[] list = linia.split(" ", 3);
                    sx = Integer.parseInt(list[0]);
                    sy = Integer.parseInt(list[1]);
                    licznik += 2;
                }

                x = 4;
                znaki = 0;
                while ((linia.length() - znaki) >= 3) {
                    znak = linia.charAt(znaki++);
                    cyfra1 = linia.charAt(znaki++);
                    cyfra2 = linia.charAt(znaki++);
                    liczba = (cyfra1 - '0') * 10 + (cyfra2 - '0');
                    switch (znak) {
                        case 'X':
                            for (int i = 0; i < liczba; ++i) {
                                SegmentT segmentT = new SegmentT(x, y);

                                x += TILESIZE;
                            }
                            break;
                        case 'A':
                            for (int i = 0; i < liczba; ++i) {
                                SegmentT segmentT = new SegmentIndestructible(x, y);
                                board.add(segmentT);
                                // segmentT.draw(textGraphics);
                                x += TILESIZE;
                            }
                            break;
                        case 'B':
                            for (int i = 0; i < liczba; ++i) {
                                SegmentT segmentT = new SegmentDestructible(x, y);
                                board.add(segmentT);
                                // segmentT.draw(textGraphics);
                                x += TILESIZE;
                            }
                            break;
                        case 'C':
                            for (int i = 0; i < liczba; ++i) {
                                SegmentT segmentT = new SegmentCoin(x, y);
                                board.add(segmentT);
                                // segmentT.draw(textGraphics);
                                x += TILESIZE;
                            }
                            break;
                        case 'E':
                            for (int i = 0; i < liczba; ++i) {
                                SegmentT segmentT = new SegmentExit(x, y);
                                board.add(segmentT);
                                // segmentT.draw(textGraphics);
                                x += TILESIZE;
                            }
                            break;


                    }

                }
                y += TILESIZE;

            }
            sprite1 = new TankSprite(sx, sy, board);
            // try {
            //  terminal.flush();
            // terminal.readInput();
            // terminal.close();
            //  } catch (IOException e) {
            //     e.printStackTrace();
            // }
            br.close();
            return board;


        } catch (IOException e) {
            System.out.println("Loading error!");
            e.printStackTrace();
            return null;
        }

    }

    public Game(String filePath) {
        board = createBoard(filePath);
    }

    public void createTerminal() {
        DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
        defaultTerminalFactory.setInitialTerminalSize(new TerminalSize(50, 30));
        try {
            terminal = defaultTerminalFactory.createTerminal();
            terminal.enterPrivateMode();
            terminal.clearScreen();
            terminal.setCursorVisible(false);
            textGraphics = terminal.newTextGraphics();
            // paint();
            createMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    // private final Sprite sprite;

    public void paint() {
        try {
            if (!painted) {
                List<SegmentT> list = board;
                for (SegmentT s : board) {
                    s.draw(textGraphics);
                    terminal.flush();
                }
                painted = true;
            }

            sprite1.draw(textGraphics);
            terminal.flush();
            long startTime = System.nanoTime();
            sprite1.setStartTime(startTime);
            //sprite2.draw(textGraphics);
            //terminal.flush();
            //new Thread(new TankController(sprite1, textGraphics)).start();
            //new Thread(new TankController(sprite2, textGraphics)).start();

            while (running) {
                if (sprite1.isGameEnded()) {
                    exitMenu();
                }

                KeyStroke key = terminal.readInput();
                switch (key.getKeyType()) {
                    case ArrowUp: {
                        sprite1.top();
                        sprite1.redraw(textGraphics);
                        sprite1.tick();
                        sprite1.draw(textGraphics);
                        terminal.flush();
                        break;
                    }
                    case ArrowDown: {
                        sprite1.bottom();
                        sprite1.redraw(textGraphics);
                        sprite1.tick();
                        sprite1.draw(textGraphics);
                        terminal.flush();
                        break;
                    }
                    case ArrowLeft: {
                        sprite1.left();
                        sprite1.redraw(textGraphics);
                        sprite1.tick();
                        sprite1.draw(textGraphics);
                        terminal.flush();
                        break;
                    }
                    case ArrowRight: {
                        sprite1.right();
                        sprite1.redraw(textGraphics);
                        sprite1.tick();
                        sprite1.draw(textGraphics);
                        terminal.flush();
                        break;
                    }
                    case Escape: {
                        terminal.close();
                        break;
                    }
                }


            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
