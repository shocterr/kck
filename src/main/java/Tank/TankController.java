package Tank;

import com.googlecode.lanterna.graphics.TextGraphics;

public class TankController implements Runnable {
	private final TankSprite tankSprite;
	private final TextGraphics textGraphics;
	//private Thread thread;
	//private Game game;
	public TankController(TankSprite sp, TextGraphics textGraphics) {
		tankSprite =sp;
		this.textGraphics = textGraphics;
	}
	/*public synchronized void start()
	{
		thread = new Thread(this);
		thread.start();
	}
	*/

	public void run() {
		while(true) {
			tankSprite.tick();
			tankSprite.draw(textGraphics);

			Thread.yield();
		}
	}
}