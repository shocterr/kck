package Tank;

import Segment.SegmentCoin;
import Segment.SegmentExit;
import Segment.SegmentT;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TankSprite {
    //private static final Image img = new ImageIcon().getImage();
    private boolean leftDirection = false; // w ktora strone patrzy
    private boolean rightDirection = false;
    private boolean topDirection = true;
    private boolean bottomDirection = false;
    //private int[] anim = {0, 1, 2, 1};
    //private int frame = 2;
    private int movingX = 0; // ruch w poziomie
    private int movingY = 0; // ruch w pionie
    private int x;
    private int y;
    private int preX = 10000;
    private int preY = 10000;
    private final int spriteWidth = 2;
    private final int spriteHeight = 2;
    private final List<SegmentT> board;
    private int points = 0;
    private long startTime;
    private long endTime;
    private boolean gameEnded;

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public boolean isGameEnded() {
        return gameEnded;
    }

    public void setGameEnded(boolean gameEnded) {
        this.gameEnded = gameEnded;
    }

    //private Plansza plansza;
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public TankSprite(int x, int y, List<SegmentT> bor) {
        this.x = x;
        this.y = y;
        board = bor;
    }

    public void draw(TextGraphics textGraphics) {

        textGraphics.drawRectangle(new TerminalPosition(x, y), new TerminalSize(spriteWidth, spriteHeight), '*');

    }

    public void redraw(TextGraphics textGraphics) {
        textGraphics.drawRectangle(new TerminalPosition(x, y), new TerminalSize(spriteWidth, spriteHeight), ' ');
    }

    public void left() {
        movingX = -2;
        movingY = 0;
        leftDirection = true;
        rightDirection = false;
        topDirection = false;
        bottomDirection = false;
    }

    public void right() {
        movingX = 2;
        movingY = 0;
        leftDirection = false;
        rightDirection = true;
        topDirection = false;
        bottomDirection = false;
    }

    public void top() {
        movingY = 2;
        movingX = 0;
        leftDirection = false;
        rightDirection = false;
        topDirection = true;
        bottomDirection = false;
    }

    public void bottom() {
        movingY = -2;
        movingX = 0;
        leftDirection = false;
        rightDirection = false;
        topDirection = false;
        bottomDirection = true;
    }

    public void stopMovingX() { //zatrzymanie na osi X
        movingX = 0;
    }

    public void stopMovingY() { //zatrzymanie na osi Y
        movingY = 0;
    }


    private boolean canGo(int dx, int dy) {
        for (SegmentT s : board)
            if (s.getBounds().intersects(x + dx, y + dy, spriteWidth, spriteHeight)) {
                return false;
            }
        return true;
    }

    private void collide(int dx, int dy)	{
        for(SegmentT s:board)
            if(s.getBounds().intersects(x+dx, y+dy, spriteWidth, spriteHeight))	{
                if (s instanceof SegmentExit) {
                    endTime = TimeUnit.SECONDS.convert(System.nanoTime() - startTime, TimeUnit.NANOSECONDS);
                    gameEnded = true;
                } else if (s instanceof SegmentCoin) {
                    SegmentCoin sCoinn =(SegmentCoin)s;
                    if(!sCoinn.isVisted()){
                        points=points+1;
                        System.out.println(this.points);
                        sCoinn.setVisted();
                    }


                }
                else if(dx != 0)
                    stopMovingX();
                else if (dy != 0)
                    stopMovingY();
            }
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void tick() {
        preX = x;
        preY = y;
        // przesuniecie w poziomie
        for (int i = 0; i < Math.abs(movingX); ++i) {
            collide((int) Math.signum(movingX), 0);
            x += ((int) Math.signum(movingX));
        }

        // przesuniecie w pionie
        for (int i = 0; i < Math.abs(movingY); ++i) {
            collide(0, -(int) Math.signum(movingY));
            y -= ((int) Math.signum(movingY));
        }
        stopMovingX();
        stopMovingY();
    }

}


