package Missile;

public class Missile {
    private MissileSprite missileSprite;
    private int damage;

    public void fire() {
        missileSprite.move();
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
